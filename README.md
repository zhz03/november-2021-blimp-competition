# November 2021 Blimp Competition

Components Needed

One ESP32 Feather Board, Stacking Header (https://www.adafruit.com/product/3405)
One Adafruit Neopixel LED Panel, Plain Header (https://www.adafruit.com/product/2945)
One Adafruit Radio FeatherWing 433MHz, Stacking Header (https://www.adafruit.com/product/3230)
Wires for I2C and motor connection
Electronic Components Mount* (in General 3D Print Design)
1 to 3 Lipo Batteries 3.7V-3.8V (https://www.amazon.com/Crazepony-Battery-PowerWhoop-Connector-Inductrix/dp/B07L9SHHFX)
One OpenMV H7 or H7 Plus (https://openmv.io/)
Velcro Tape**
Two Blimp*** (https://www.amazon.com/Swimmers-Remote-Control-Flying-Clownfish/dp/B005FYCBR6/ref=sr_1_5?crid=1QSXYA4FQYTC1&dchild=1&keywords=air+swimmers+remote+control+flying+fish&qid=1629779292&sprefix=air+swimmers%2Caps%2C229&sr=8-5)
Two 2.2kOhms resistors
Helium
Three DC motors (https://www.amazon.com/Hobbypower-Coreless-53000rpm-8-5x20mm-Propeller/dp/B076M7G24G/ref=pd_sbs_6/140-9159272-8519647?pd_rd_w=TKtuN&pf_rd_p=0f56f70f-21e6-4d11-bb4a-bcdb928a3c5a&pf_rd_r=VR3D6QW63FJ4FG19JE97&pd_rd_r=c10d4879-cb04-4453-87fe-8cfa37d04396&pd_rd_wg=7TWip&pd_rd_i=B076M7G24G&psc=1)
Three Propellers (https://www.amazon.com/Hobbypower-Coreless-53000rpm-8-5x20mm-Propeller/dp/B076M7G24G/ref=pd_sbs_6/140-9159272-8519647?pd_rd_w=TKtuN&pf_rd_p=0f56f70f-21e6-4d11-bb4a-bcdb928a3c5a&pf_rd_r=VR3D6QW63FJ4FG19JE97&pd_rd_r=c10d4879-cb04-4453-87fe-8cfa37d04396&pd_rd_wg=7TWip&pd_rd_i=B076M7G24G&psc=1)
Putty (https://www.amazon.com/Removable-Adhesive-Non-Toxic-Multipurpose-Photography/dp/B08DTHJTSZ/ref=sr_1_9?crid=38S8BXT4S632I&dchild=1&keywords=sticky+clay+adhesive+putty&qid=1629782480&sprefix=sticky+clay%2Ctoys-and-games%2C221&sr=8-9)

*: Although we are currently using a 3D-printed gondola as a mount, anything else that can hold the components together can be used.
**: velcros are used to hold the mount to the blimp. If you don't have it, normal tape would suffice but it will be a little bit messier
***: The total mass of all components weight approximately around 120g. Therefore, a blimp that can carry that much weight is needed. For the Amazon product listed, you need two of them for the blimp to float in the air.
Connecting the ESP32 Feather with OpenMV

If you are planning on using the I2C communication protocol, you need to connect the following from Feather to OpenMV:

GND pin - GND pin
SCL/22 pin - P4 pin
SDA/23 pin - P5 pin


Just like the ESP32, we need to make sure the battery connected to OpenMV is of the right polarity. With the battery suggested, you need to reverse the wiring in order to connect a battery to OpenMV.
If the battery is properly connected, the OpenMV LED will blink white at the start. In addition to that, a debugging feature was added such that the OpenMV LED will be red once it is switched on. Everytime a function is called, it will blink green for the period of the function.
In addition to that, since OpenMV is not a stackable feather component, you need to add pull up resistors for both the SDA and SCL lines. The way to do this is by connecting two 2.2kOhms resistors where one end is connected to ESP32 3V and the other one is connected to the SDA and SCL respectively.

Pullup resistor circuit example:

(https://www.analog.com/en/technical-articles/i2c-primer-what-is-i2c-part-1.html)
Camera setup example:

Note: Failure to properly setup the I2C protocol might cause the code to not work properly
Connecting the ESP32 Feather with the Motor Shield

The Feather and the shield should be stackable. Make sure you have soldered the pins properly.
The shield runs on a separate power source. Make sure that you have a 3.7V Lipo Battery if you are planning to use the shield.
If the battery is properly connected, a green LED should light up. This LED also approximately indicate how much power the battery has left as it will get dimmer with lower voltage.
Connect the vertical motor to M1, right motor to M2, and left motor to M4.
The motor itself can be connected  differently if you are using a different vehicle. If so, changes might be needed for the motor controller code section.

Note: depending on the wiring and the propeller design, the propeller might push the blimp in the opposite direction as the code intended. Ways to solve this problem include reversing the polarity wiring of the motor, use the oppsite-direction propeller of the one you have right now (if it is clockwise, use the counter-clockwise propeller instead), or change the code so that all BACKWARD motor calls are FORWARD and vice versa.
DC Motor Shield setup example:

Other Physical Setup

The stack, starting from the bottom is the ESP32 Feather, Radiowing Feather, DC Motor Shield, and the Neopixel LED Panel.
Just like the OpenMV, reverse the wiring before connecting a battery to the ESP32. Optional: if weight is an issue, you can use a single battery and connect the ESP32 and OpenMV in parallel. However, it is strongly advised that you use a separate battery for the DC Shield as it consumes a lot of power.
Fill the two blimp balloons with Helium until both of them together can carry around 130-140g. In order to test this, tie a string to the blimp and attach the string to a heavier object like a smartphone. First, weight the smartphone by itself. Then, weight the smartphone when it is attached to the string. The difference will be what the blimp can carry.
Using the velro tape, attach the top of the electronic mount to the bottom of the blimp
Similarly, use velcro tape to attach the OpenMV camera to the front of the mount in such a way that the camere is facing the forward direction.
Plug the motor to its corresponding location on the mount
Place the ESP32 and the rest of the stack into the mount
Test to see if the whole blimp floats weightlessly in the air. If it is falling down, add more helium to the blimp balloon. If it is floating up, add putty into the mount. Repeat this until it approximately stays still in the air without any external force acting on it.

Blimp stack setup:

Blimp front view:

Blimp bottom view:

Complete setup:

Setting Up the General Libraries and Codes

Go to https://git.uclalemur.com/arnhold/espmeshnetworking/-/tree/master/MeshCode

In that repository, follow the instructions to install the ArnholdMesh.zip and Painless_Mesh.zip libraries.
If you are planning on doing demo using the LED panel, please also install Adafruit Neopixel library through the Library Manager in your Arduino IDE
If you are using the Adafruit Motor shield, do not install the default library through the library manager. Instead, in your Arduino IDE, go to Sketch > Include Library > Add .ZIP Library
Download or Clone the Adafruit_Motor_Shield_V2_Library.zip from this repository and use that for Step 4. The reason we are using an alternate library from the default one is because the motor library and the mesh library uses the same name for one of their macros. This changes the macro's name in the motor library to ensure that this doesn't happen.
Download the PID library, which is needed for the motion control. (https://playground.arduino.cc/Code/PIDLibrary/)

Running the Code

Create a folder name feather_main_personaldemo in your directory of choice.
Copy everthing in the Main Code repository into the feather_main_personaldemo folder except for OpenMV_main.py, the motor library, and the other ino file that is not feather_main_personaldemo.
Open feather_main_personaldemo.ino and upload the sketch into your ESP32 Feather
Upload the OpenMV_main.py into your OpenMV camera as main.py so that it can run without being connected to the IDE
By default, it should start detecting and responding to green blob.

Varying the Parameters
If we want to vary the behaviour of the blimp, there are a couple of parameter that we can use to do that. The section below explains what message needs to reach the ESP32 through the mesh network. However, the methodology of how the message is sent to the blimp node is completely up to you. Another section further below will give you an example remote controller you can use (although not the best)


Color Detection Threshold (C)

By default, the blimp will start trying to detect the biggest green blob. However, there are 2 ways we can change the threshold so that the program will look for other colored blobs.
The first way is to provide a single-digit color code. The code along with the command character:

"C1": generic green blob
"C2": generic blue blob
"C5": generic red blob


If the generic colors are not enough, you can also prescribe a specific thresholds for the robot to use. The format will be something like:

"C(Lmin) (Lmax) (Amin) (Amax) (Bmin) (Bmax)", without the brackets
"C30 100 12 -19 0 -120"





PID Constants (T)

You can also vary the PID constant for the horizontal motion. By default, these are the constants:

Kpx = 1
Kix = 0.05
Kdx = 0.25


In order to change the constant which are double datatype, the generic form of the message should be:

"TP0.1 I0.2 D0.3"


For this command, you can also just change one or 2 parameters by doing the command like:

"TP0.5"
"TI0.9 D1.5"





Pause the Blimp (P)

If you want to stop the mesh from moving, a quick way is to use the pause feature:

"P1": pause the blimp program
"P0": resume the blimp program





Change the Base Speed (S)

The base speed is used to move towards the tracked object. If it is 0, it the blimp will not actively move towards the object if it is detected by the camera. The command format is:

"S0"
"S250"





LED Panel Debugging with the Blimp

OpenMV LED: blink green when a function is called. red on standby
PIN31: state of the blimp. red is standby, yellow is paused, white is seeking, blue is done
PIN30: kp change. blink green if new kp is higher then old kp. red if less. white if the same
PIN29: ki change. blink green if new ki is higher then old ki. red if less. white if the same.
PIN28: kd change. blink green if new kd is higher then old kd. red if less. white if the same
PIN27: base speed indicator. the white LED intensity scale with the base speed, with 255 speed being the brightest
PIN26: minimum threshold for color detection, display the minimum LAB value in terms of RGB
PIN25: maximum threshold for color detection, display the maximum LAB value in terms of RGB
PIN24: left motor speed. display the speed of the motor that scales with the light intensity. led will be red if the motor is pushing the blimp forward, blue if backward
PIN17: vertical motor speed. display the speed of the motor that scales with the light intensity. led will be red if the motor is pushing the blimp upward, blue if downward
PIN16: right motor speed. display the speed of the motor that scales with the light intensity. led will be red if the motor is pushing the blimp forward, blue if backward

Using the Camera.cpp Class Separately


Install OpenMV Arduino RPC Library into your Arduino IDE.


At the top of your main code, include the following lines to create the i2c interface that the OpenMV will communicate with the feather:
 #include "Camera.h"
 openmv::rpc_scratch_buffer<256> scratch_buffer; // All RPC objects share this buffer.
 openmv::rpc_i2c_master interface(0x12, 100000); //to make this more robust, consider making address and rate as constructor argument


You need to pass the interface's address when you are constructing the camera:
 Camera cam(&interface);


Now, you should be able to use all the function in the Camera.h file.


If you want to make a new function in the OpenMV and create an interfacing function corresponding to it on Arduino, please refer the RPC library for further clarification. https://github.com/openmv/openmv-arduino-rpc
