#include <esp_now.h>
#include <WiFi.h>
#include <Wire.h>

// REPLACE WITH THE MAC Address of your receiver 
uint8_t broadcastAddress[] = {0xC4, 0xDD, 0x57, 0x9E, 0x91, 0x74};
String success;
// Define variables to store BME280 readings to be sent
String strdata = "";
String valdata = "";
int count = 0;

// Define variables to store incoming readings
String incomingStr = "";
double incomingVal = 0;
String incomingDebug = "";

// =====================================================================================
//Structure the sending data
//Must match the receiver structure
typedef struct struct_message {
  String StrD;
  double ValD;
  String DebugM;
} struct_message;

// Create a struct_message to hold incoming sensor readings
// struct_message incomingReadings;
struct_message sentData;
// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  if (status ==0){
    success = "Delivery Success :)";
  }
  else{
    success = "Delivery Fail :(";
  }
}

// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&sentData, incomingData, sizeof(sentData));
  // Serial.print("Bytes received: ");
  // Serial.println(len);
  // incomingDebug = incomingReadings.DebugM;
  Serial.print("Debug message:");
  Serial.println(sentData.DebugM);
}
// =====================================================================================

void setup() {
  // Init Serial Monitor
  Serial.begin(9600);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);
}

// =====================================================================================
void loop()
{ 
  while (Serial.available()>0){
   int inChar = Serial.read();
   strdata += char(inChar);
   delay(10);
   count +=1;
   if (count == 3){ 
    sentData.StrD = strdata;
    Serial.println(sentData.StrD);
   }
  if (isDigit(inChar) || inChar == '.' || inChar == '-'){
    valdata += char(inChar);
   }
  
  if (inChar == '\n'){ //after message is sent
   sentData.ValD = valdata.toDouble();
   Serial.println(sentData.ValD);
   
  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &sentData, sizeof(sentData));
  // esp_now_send(RxMACaddress, (uint8_t *) &sentData, sizeof(sentData));
  //-------------------------------------------------------------------------------------
  if (result == ESP_OK) Serial.println("Sent with success");
  else Serial.println("Error sending the data");
  //-------------------------------------------------------------------------------------
  delay(500);
  strdata = "";
  valdata = "";
  count = 0;
  }
  
  }

  //-------------------------------------------------------------------------------------
  
}
