//ESP-NOW: Receiver
//Ref: Random Nerd Tutorials https://randomnerdtutorials.com
//-----------------------------------------------------------
#include <esp_now.h>
#include <WiFi.h>
#include <Wire.h>

//-------------------------------------------------------------------------------------
String strData = "";
double valData = 0.0;
double Kpx=2, Kix=0.1, Kdx=0.25;
double Kpy=1, Kiy=0.1, Kdy=0.25;
//-------------------------------------------------------------------------------------
typedef struct RxStruct
{
  String StrD;
  double ValD;
}RxStruct;
RxStruct receivedData;
//-------------------------------------------------------------------------------------
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len)
{
  memcpy(&receivedData, incomingData, sizeof(receivedData));
  Serial.print("data:");
  Serial.println(receivedData.StrD);
  Serial.println(receivedData.ValD);
  strData = receivedData.StrD;
  valData = receivedData.ValD;
}
//======================================================================================
void setup()
{
  Serial.begin(9600);
  
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != ESP_OK)
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  esp_now_register_recv_cb(OnDataRecv);
}
//======================================================================================
void loop()
{ 
  Kpx = getVal(strData,"kpx",valData,Kpx);
  Kix = getVal(strData,"kix",valData,Kix);
  Kdx = getVal(strData,"kdx",valData,Kdx);
  Kpy = getVal(strData,"kpy",valData,Kpy);
  Kiy = getVal(strData,"kiy",valData,Kiy);
  Kdy = getVal(strData,"kdy",valData,Kdy);
  
    Serial.print("Kpx:");
    Serial.println(Kpx);
    Serial.print("Kix:");
    Serial.println(Kix);
    Serial.print("Kdx:");
    Serial.println(Kdx);
    Serial.print("Kdy:");
    Serial.println(Kdy);
    Serial.print("Kiy:");
    Serial.println(Kiy);
    Serial.print("Kdy:");
    Serial.println(Kdy);

}

double getVal(String checkData,String Ans,double val,double ori_val){
  if (checkData == Ans){
    strData = "";
    valData = 0.0;
    return val;
  }else {
    return ori_val;
  }
}
