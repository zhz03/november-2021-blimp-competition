//-----------------------------------------------------------
//ESP-NOW: Transmitter
//Ref: Random Nerd Tutorials https://randomnerdtutorials.com
//-----------------------------------------------------------
#include <esp_now.h>
#include <WiFi.h>
//-------------------------------------------------------------------------------------
uint8_t RxMACaddress[] = {0xC4, 0xDD, 0x57, 0x9E, 0x91, 0x74};
String strdata = "";
String valdata = "";
int count = 0;
//-------------------------------------------------------------------------------------
typedef struct TxStruct
{
  String StrD;
  double ValD;
}TxStruct;
TxStruct sentData;
//-------------------------------------------------------------------------------------
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) //callback function
{
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
//======================================================================================
void setup()
{
  Serial.begin(9600);
  WiFi.mode(WIFI_STA);
  //------------------------------------------------------------------------------------
  if(esp_now_init() != ESP_OK)
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  //-------------------------------------------------------------------------------------
  esp_now_register_send_cb(OnDataSent);
  //-------------------------------------------------------------------------------------
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, RxMACaddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  //-------------------------------------------------------------------------------------
  if(esp_now_add_peer(&peerInfo) != ESP_OK)
  {
    Serial.println("Failed to add peer");
    return;
  }
}
//======================================================================================
void loop()
{ 
  while (Serial.available()>0){
    int inChar = Serial.read();
    /*
    sentData.potVal = inChar - '0';
    delay(10);
    esp_now_send(RxMACaddress, (uint8_t *) &sentData, sizeof(sentData));
    delay(500);
    */
   strdata += char(inChar);
   delay(10);
   count +=1;
   if (count == 3){
    sentData.StrD = strdata;
    Serial.println(sentData.StrD);
   }
  if (isDigit(inChar) || inChar == '.'){
    valdata += char(inChar);
  }
  
  if (inChar == '\n'){
   sentData.ValD = valdata.toDouble();
   Serial.println(sentData.ValD);
  esp_err_t result = esp_now_send(RxMACaddress, (uint8_t *) &sentData, sizeof(sentData));
  // esp_now_send(RxMACaddress, (uint8_t *) &sentData, sizeof(sentData));
  //-------------------------------------------------------------------------------------
  if (result == ESP_OK) Serial.println("Sent with success");
  else Serial.println("Error sending the data");
  //-------------------------------------------------------------------------------------
  delay(500);
  strdata = "";
  valdata = "";
  count = 0;
  }
  
  }

  //-------------------------------------------------------------------------------------
  
}
