//ESP-NOW: Receiver
//Ref: Random Nerd Tutorials https://randomnerdtutorials.com
//-----------------------------------------------------------
#include <esp_now.h>
#include <WiFi.h>
#include <Wire.h>

//-------------------------------------------------------------------------------------
String strData = "";
double valData = 0.0;
double Kpx=2, Kix=0.1, Kdx=0.25;
double Kpy=1, Kiy=0.1, Kdy=0.25;
int g1 = 0,g2=1,g3=2;
int goal_id[3] = {g1, g2, g3};
int8_t Lmin = 30,Lmax = 100, Amin = -49,Amax = -22,Bmin = 31,Bmax = 127;
int8_t threshold[6] = {Lmin, Lmax, Amin, Amax, Bmin, Bmax};
int base_speed = 70;
int seeking_speed = 70;
int lidar_thres = 300; // mm 
//-------------------------------------------------------------------------------------
typedef struct RxStruct
{
  String StrD;
  double ValD;
}RxStruct;
RxStruct receivedData;
//-------------------------------------------------------------------------------------
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len)
{
  memcpy(&receivedData, incomingData, sizeof(receivedData));
  Serial.print("data:");
  Serial.println(receivedData.StrD);
  Serial.println(receivedData.ValD);
  strData = receivedData.StrD;
  valData = receivedData.ValD;
}
//======================================================================================
void setup()
{
  Serial.begin(9600);
  
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != ESP_OK)
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  esp_now_register_recv_cb(OnDataRecv);
}
//======================================================================================
void loop()
{   ChangeVariables();
     Serial.print("threshold:");
    Serial.print(threshold[0]);
    Serial.print("|");
    Serial.print(threshold[1]);
    Serial.print("|");
    Serial.print(threshold[2]);
    Serial.print("|");
    Serial.print(threshold[3]);
    Serial.print("|");
    Serial.print(threshold[4]); 
    Serial.print("|");   
    Serial.println(threshold[5]);  
}

void ChangeVariables(){ 
  // all the variables in this function can be remotely changed
    //-------------------PID-----------------
  Kpx = getDoubleVal(strData,"kpx",valData,Kpx);
  Kix = getDoubleVal(strData,"kix",valData,Kix);
  Kdx = getDoubleVal(strData,"kdx",valData,Kdx);
  Kpy = getDoubleVal(strData,"kpy",valData,Kpy);
  Kiy = getDoubleVal(strData,"kiy",valData,Kiy);
  Kdy = getDoubleVal(strData,"kdy",valData,Kdy);
  //-------------------Goal id-----------------
  g1 = getIntVal(strData,"gda",valData,goal_id[0]);
  g2 = getIntVal(strData,"gdb",valData,goal_id[1]);
  g3 = getIntVal(strData,"gdc",valData,goal_id[2]);
  goal_id[0] = g1;
  goal_id[1] = g2;
  goal_id[2] = g3;
  //-------------------Color threshold-----------------
  Lmin = getIntVal(strData,"tha",valData,threshold[0]);
  Lmax = getIntVal(strData,"thb",valData,threshold[1]);
  Amin = getIntVal(strData,"thc",valData,threshold[2]);
  Amax = getIntVal(strData,"thd",valData,threshold[3]);
  Bmin = getIntVal(strData,"the",valData,threshold[4]);
  Bmax = getIntVal(strData,"thf",valData,threshold[5]);
  threshold[0] = Lmin;
  threshold[1] = Lmax;
  threshold[2] = Amin;
  threshold[3] = Amax;
  threshold[4] = Bmin;
  threshold[5] = Bmax;  
  //-------base_speed,seeking_speed,lidar_thres-----------------
  base_speed = abs(getIntVal(strData,"bsp",valData,base_speed));
  seeking_speed = abs(getIntVal(strData,"ssp",valData,seeking_speed));
  lidar_thres = abs(getIntVal(strData,"lth",valData,lidar_thres));
  
     /*
    Serial.print("base speed:");
    Serial.print(base_speed);
    Serial.print("|");
    Serial.print("seeking speed:");
    Serial.print(seeking_speed);
    Serial.print("|");
    Serial.print("lidar thres:");
    Serial.println(lidar_thres);
    */
    /*
    Serial.print("threshold:");
    Serial.print(threshold[0]);
    Serial.print("|");
    Serial.print(threshold[1]);
    Serial.print("|");
    Serial.print(threshold[2]);
    Serial.print("|");
    Serial.print(threshold[3]);
    Serial.print("|");
    Serial.print(threshold[4]); 
    Serial.print("|");   
    Serial.println(threshold[5]);  
     */

   /*
    Serial.print("gid:");
    Serial.print(goal_id[0]);
    Serial.print(goal_id[1]);
    Serial.println(goal_id[2]);
  */
    /*
    Serial.print("Kpx:");
    Serial.println(Kpx);
    Serial.print("Kix:");
    Serial.println(Kix);
    Serial.print("Kdx:");
    Serial.println(Kdx);
    Serial.print("Kdy:");
    Serial.println(Kdy);
    Serial.print("Kiy:");
    Serial.println(Kiy);
    Serial.print("Kdy:");
    Serial.println(Kdy);
  */
}

double getDoubleVal(String checkData,String Ans,double val,double ori_val){
  if (checkData == Ans){
    strData = "";
    valData = 0.0;
    return val;
  }else {
    return ori_val;
  }
}

int getIntVal(String checkData,String Ans,double val,int ori_val){
  if (checkData == Ans){
    strData = "";
    valData = 0.0;
    
    return (int8_t)val;
  }else {
    return ori_val;
  }
}
