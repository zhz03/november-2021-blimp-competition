#ifndef CAMERADEFN_H
#define CAMERADEFN_H

#include "Constants.h"
#include "Camera.h"

openmv::rpc_scratch_buffer<256> scratch_buffer; // All RPC objects share this buffer.
openmv::rpc_i2c_master interface(0x12, 100000); //to make this more robust, consider making address and rate as constructor argument

Camera cam(&interface);

void CameraDefn_Setup(){
  interface.begin(); //communication between ESP and OpenMV
}

bool CameraDefn_BallDetect(int& x, int& y){
  return cam.exe_color_detection_biggestblob(threshold[0], threshold[1], threshold[2], threshold[3], threshold[4], threshold[5], x, y);
}

float CameraDefn_GoalFind(int8_t goal[3], int id, int tx, int ty, int tz, int rx, int ry, int rz){
  return cam.exe_goalfinder(goal[0], goal[1], goal[2], id, tx, ty, tz, rx, ry, rz);
}

#endif
