#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <vector>

// Identify all the global constants that will be used by the robot
const bool ML_BALL_DETECT = true;
const int BAUD_RATE = 115200;
const int MAX_SPEED = 255;
const int SEEKING_SPEED = 70;
const double RESOLUTION_W = 320.0;
const double RESOLUTION_H = 240.0;
const int ENDDEMO_TAG = 0;
const uint32_t THISNODE_ID = 88789821;
int BASE_SPEED = 50; //125;
int GROUND_LEVEL = 1000; // Pressure at ground level
int BALL_DETECT_GAP = 500;

// The debug LEDs that we will be using. Description at:
const int DEBUG_STATE = 31;
const int DEBUG_KP = 30;
const int DEBUG_KI = 29;
const int DEBUG_KD = 28;
const int DEBUG_BASESPEED = 27;
const int DEBUG_THRESHOLD_MIN = 26;
const int DEBUG_THRESHOLD_MAX = 25;
const int DEBUG_VERTICALSPEED = 17;
const int DEBUG_RSPEED = 16;
const int DEBUG_LSPEED = 24;

//int8_t threshold[6] = {0, 0, 0, 0, 0, 0};

#endif
