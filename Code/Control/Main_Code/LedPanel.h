#ifndef LEDPANEL_H
#define LEDPANEL_H

#include <Adafruit_NeoPixel.h>
#define NUMPIXELS 32
#define PIN_PIXELS 32 // Use pin 32 for NeoPixels
const int HORIZONTAL_SIZE = 8;
const int VERTICAL_SIZE = 4;

class LedPanel {
  private:
  int led_num;
  Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_PIXELS, NEO_GRB + NEO_KHZ800);
  
  public:
  LedPanel(int pixelnum, int pixelpin);
  void fullPanelLight(int r, int b, int g);
  void resetPanel();
  void topLeftQuadrant(int r, int g, int b);
  void bottomLeftQuadrant(int r, int g, int b);
  void topRightQuadrant(int r, int g, int b);
  void bottomRightQuadrant(int r, int g, int b);
  void beginPanel();
  void singleLED(int num, int r, int g, int b);
  
};

#endif
