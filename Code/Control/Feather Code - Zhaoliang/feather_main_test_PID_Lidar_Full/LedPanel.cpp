#include "LedPanel.h"

LedPanel::LedPanel(int pixelnum, int pixelpin){
  led_num = pixelnum;
  //pixels = Adafruit_NeoPixel(pixelnum, pixelpin, NEO_GRB + NEO_KHZ800);
}

void LedPanel::fullPanelLight(int r, int g, int b){
  for (int i=0 ; i<led_num ; i++){
    pixels.setPixelColor(i, r,g,b);
  }
  pixels.show();
}

void LedPanel::beginPanel(){
  pixels.begin();
}

void LedPanel::resetPanel(){
  for (int i=0 ; i<led_num ; i++){
    pixels.setPixelColor(i, 0,0,0);
  }
  pixels.show();
}

void LedPanel::topLeftQuadrant(int r, int g, int b){
  for (int i = 0; i<HORIZONTAL_SIZE/2; i++){
      for (int j=0; j<VERTICAL_SIZE/2;j++){
        pixels.setPixelColor(i+j*HORIZONTAL_SIZE, r, g, b);
      }
  }
  pixels.show();
}

void LedPanel::bottomLeftQuadrant(int r, int g, int b){
    for (int i = 0; i<HORIZONTAL_SIZE/2; i++){
      for (int j=VERTICAL_SIZE/2; j<VERTICAL_SIZE;j++){
        pixels.setPixelColor(i+j*HORIZONTAL_SIZE, r, g, b);
      }
  }
  pixels.show();
}

void LedPanel::topRightQuadrant(int r, int g, int b){
  for (int i = HORIZONTAL_SIZE/2; i<HORIZONTAL_SIZE; i++){
    for (int j=0; j<VERTICAL_SIZE/2;j++){
      pixels.setPixelColor(i+j*HORIZONTAL_SIZE, r, g, b);
    }
  }
  pixels.show();
}

void LedPanel::bottomRightQuadrant(int r, int g, int b){
  for (int i = HORIZONTAL_SIZE/2; i<HORIZONTAL_SIZE; i++){
    for (int j=VERTICAL_SIZE/2; j<VERTICAL_SIZE;j++){
      pixels.setPixelColor(i+j*HORIZONTAL_SIZE, r, g, b);
    }
  }
  pixels.show();  
}

void LedPanel::singleLED(int num, int r, int g, int b) {
    pixels.setPixelColor(num, r, g, b);
    pixels.show();
}
