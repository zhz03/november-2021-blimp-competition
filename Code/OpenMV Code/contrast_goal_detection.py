import sensor, image, time, pyb

from pyb import Pin
ir_led = pyb.LED(4)
red_led = pyb.LED(1)
b_led = pyb.LED(2)
g_led = pyb.LED(3)
ir_led.off()
ir_led.on()
red_led.off()

sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE) # grayscale is faster (160x120 max on OpenMV-M7)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time = 2000)
sensor.set_auto_gain(False)  # must turn this off to prevent image washout...
sensor.set_auto_whitebal(False)  # must turn this off to prevent image washout...
sensor.set_auto_exposure(False, 10000)
clock = time.clock()
w = sensor.width()
h = sensor.height()
MIN_DIFFERENCE = 15


while(True):
    ir_led.off()
    img_off = sensor.snapshot()
    ir_led.on()
    img_on = sensor.snapshot()
    xmin = w
    xmax = 0
    ymin = h
    ymax = 0
    maxval = 0

    for i in range(w):
        for j in range(h):
            temp = img_on.get_pixel(i, j) - img_off.get_pixel(i,j)
            if (temp > maxval): maxval = temp
            if (temp > MIN_DIFFERENCE):
                if (i > xmax): xmax = i
                if (i < xmin): xmin = i
                if (j > ymax): ymax = j
                if (j < ymin): ymin = j
    width = xmax - xmin
    height = ymax - ymin
    x = xmin + width/2
    y = ymin + height/2
    img_on.draw_rectangle(int(xmin), int(ymin), int(width), int(height))
    print("x: ", x, " y: ", y)
    print("highest difference: ", maxval)





