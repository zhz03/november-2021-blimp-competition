import urllib.request
import cv2
import numpy as np

if __name__ == '__main__':

    frame = None
    key = None
    url = 'http://192.168.1.117/capture?'
    # url = 'http://192.168.1.117:81/stream'
    print('Start')
    while True:
        imgResponse = urllib.request.urlopen(url)
        imgNp = np.array(bytearray(imgResponse.read()),dtype=np.uint8)
        frame = cv2.imdecode(imgNp,-1)
        cv2.imshow('Window',frame)
        h, w, _ = frame.shape
        print("with:{},high:{}".format(w, h))
        key = cv2.waitKey(5)
        if key == ord('q'):
            break

    cv2.destroyAllWindows()
