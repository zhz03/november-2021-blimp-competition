# -*- coding: utf-8 -*-
"""
color-segmentation-8.ipynb
https://colab.research.google.com/drive/1rm59sXDX-pAYmIEGpNN7h-8WIYe8luPb
"""

## Codebase - Live Training-based Ball Detection (IN PROGRESS)
### Import Modules and Drive
import os
import cv2
import torch
import numpy as np
import torch.nn as nn
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from base64 import b64decode
from datetime import datetime
from torchvision import transforms
from detecto import core, utils, visualize
from IPython.display import Image, display, Javascript
# print("torch.cuda.is_available() = ", torch.cuda.is_available())

mainDir     = './'
imageLoc    = './images/train-test-1/'
trainLoc    = imageLoc + 'train/'
modelLoc    = mainDir  + 'detecto-models-weights/'
modelAction = 'load'
modelFile   = 'model_weights-20210728_215304.pth'
minScore    =   0.75
cropFactor  =   0.90
colorLow    =  60
colorHigh   = 180
labelSet    = ['ball']

### Train the Model
if modelAction == 'new':
  dataset = core.Dataset(trainLoc)
  model   = core.Model(labelSet)
  losses  = model.fit(dataset, epochs = 20, verbose = True)
  time    = datetime.now().strftime("%Y%m%d_%H%M%S")
  model.save(modelLoc + 'model_weights-' + time + '.pth')
elif modelAction == 'load':
  model = core.Model.load(modelLoc + modelFile, labelSet)

### Object Detection
def objectDetection(loc):
  image = utils.read_image(loc)
  predictions = model.predict(image)
  tLabels, tBoxes, tScores = predictions
  labels,  boxes,  scores  = [[] for i in range(3)]
  
  imHeight, imWidth, _ = image.shape
  for count, box in enumerate(tBoxes):
    x0, y0 = float(box[0]), float(box[1])
    x1, y1 = float(box[2]), float(box[3])
    w      = x1 - x0
    h      = y1 - y0
    center = (x0 + (w/2), y0 + (h/2))
    dCrop  = [int(y0*cropFactor + y1*(1-cropFactor)), int(y1*cropFactor + y0*(1-cropFactor)),
              int(x0*cropFactor + x1*(1-cropFactor)), int(x1*cropFactor + x0*(1-cropFactor))]
    relativeDist, coordinates = depth_estimate((imHeight, imWidth), (x0, y0, w, h))
    relativeDist = float(relativeDist)
    coordinates  = tuple([float(coord) for coord in coordinates])
    avgClRGB     = cv2.mean(image[dCrop[0]:dCrop[1], dCrop[2]:dCrop[3]])
    avgClHue     = rgbToHue(avgClRGB)
    print('Detection Score     : ', round(float(tScores[count])*100, 2), '%')
    print('Average Color (RGB) : ', avgClRGB)
    print('Average Color (Hue) : ', avgClHue)
    print('Bounding Box Center : ', center)
    print('Relative Distance   : ', relativeDist)
    print('Coordinates         : ', coordinates)
    print()
    if tScores[count] > minScore and avgClHue > colorLow and avgClHue < colorHigh:
      scores.append(float(tScores[count]))
      labels.append(tLabels[count]+' '+str(round(float(tScores[count])*100, 2))+'%')
      boxes.append ([x0, y0, x1, y1])
  displayBoxedImage(image, boxes, labels)

#### Live Detection
def take_photo(filename='photo.jpg', quality=0.8):
  js = Javascript('''
    async function takePhoto(quality) {
      const div = document.createElement('div');
      const capture = document.createElement('button');
      capture.textContent = 'Capture';
      div.appendChild(capture);

      const video = document.createElement('video');
      video.style.display = 'block';
      const stream = await navigator.mediaDevices.getUserMedia({video: true});

      document.body.appendChild(div);
      div.appendChild(video);
      video.srcObject = stream;
      await video.play();

      // Resize the output to fit the video element.
      google.colab.output.setIframeHeight(document.documentElement.scrollHeight, true);

      // Wait for Capture to be clicked.
      await new Promise((resolve) => capture.onclick = resolve);

      const canvas = document.createElement('canvas');
      canvas.width = video.videoWidth;
      canvas.height = video.videoHeight;
      canvas.getContext('2d').drawImage(video, 0, 0);
      stream.getVideoTracks()[0].stop();
      div.remove();
      return canvas.toDataURL('image/jpeg', quality);
    }
    ''')
  display(js)
  data = eval_js('takePhoto({})'.format(quality))
  binary = b64decode(data.split(',')[1])
  print(data)
  with open(filename, 'wb') as f:
    f.write(binary)
  return filename

def detectLive1(model):
  try:
    filename = take_photo()
    print('Saved to {}'.format(filename))
    frame = Image(filename)
    labels, boxes, scores = model.predict(frame)
    cv2.putText(frame, 'Test', (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 3)
    for i in range(boxes.shape[0]):
      if scores[i] < minScore:
        continue
      box = boxes[i]
      cv2.rectangle(frame, (int(box[0]), int(box[1])),
                           (int(box[2]), int(box[3])),
                           (255, 0, 0), 3)
      if labels:
        cv2.putText(frame, '{}: {}'.format(labels[i], round(scores[i].item(), 2)),
                           (int(box[0]), int(box[1]) - 10),
                           cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 3)
    display(frame)
  except Exception as err:
    print(str(err))

def detectLive2(model):
  cv2.namedWindow('Ball Detection')
  try:
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
  except:
    print('No webcam available.')
    return
  
  ret = True
  while ret:
    ret, frame = cap.read()
    
    labels, boxes, scores = model.predict(frame)
    # labels, boxes, scores = [np.array([]) for i in range(3)]
    for i in range(boxes.shape[0]):
      if scores[i] < minScore:
        continue
      box = boxes[i]
      cv2.rectangle(frame, (int(box[0]), int(box[1])),
                           (int(box[2]), int(box[3])),
                           (255, 0, 0), 3)
      if labels:
        cv2.putText(frame, '{}: {}'.format(labels[i], round(scores[i].item(), 2)),
                           (int(box[0]), int(box[1]) - 10),
                           cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 3)

    cv2.imshow('Ball Detection', frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('q') or key == 27:
      break

  cv2.destroyAllWindows()
  cap.release()

### Supplementary Functions
#### Display of Image with Bounding Boxes
def displayBoxedImage(image, boxes = [], labels = None):
  fig, ax = plt.subplots(1)
  if isinstance(image, torch.Tensor):
    image = utils.reverse_normalize(image)
    image = transforms.ToPILImage()(image)
  ax.imshow(image)

  if labels is not None and not utils._is_iterable(labels):
    labels = [labels]

  for i in range(len(boxes)):
    box = boxes[i]
    width, height =  box[2] - box[0], box[3] - box[1]
    initial_pos   = (box[0], box[1])
    rect = patches.Rectangle(initial_pos, width, height, linewidth = 1, edgecolor = 'r', facecolor = 'none')
    if labels:
      ax.text(box[0] + 5, box[1] - 5, '{}'.format(labels[i]), color='red')
    ax.add_patch(rect)
  
  plt.show()

#### Calculation of Hue from RGB (adopted from [code](https://www.geeksforgeeks.org/program-change-rgb-color-model-hsv-color-model/))
def rgbToHue(RGB):
  RGB = [val/255.0 for val in RGB]

  cmax = max(RGB)
  cmin = min(RGB)
  diff = cmax - cmin

  r, g, b = RGB[0:3]
  if cmax == cmin:
    return 0
  if cmax == r:
    return (60 * ((g - b) / diff) + 360) % 360
  if cmax == g:
    return (60 * ((b - r) / diff) + 120) % 360
  if cmax == b:
    return (60 * ((r - g) / diff) + 240) % 360

#### Depth Estimation (adopted from [mono_depth.py](https://github.com/agrawalparth10/FORAY-Perception/blob/master/mono_depth/mono_depth.py))
def depth_estimate(figDims, boundingBox):
  def x_hat(realLen, ip, i0, imageLen):
    return (realLen * (ip-i0)) / imageLen

  def y_hat(realLen, jp, j0, imageLen):
    return (realLen * (jp-j0)) / imageLen
  
  def z_hat(dist, pixelLen, imageLen):
    return dist*(pixelLen/imageLen)
  
  def cal_coordinates(dist, realLen, ip, jp, i0, j0, imageLen, pixelLen):
    return (x_hat(realLen, ip, i0, imageLen), y_hat(realLen, jp, j0, imageLen), z_hat(dist, pixelLen, imageLen))
  
  def measure(dist, realLen, ip, jp, i0, j0, imageLen, pixelLen):
    x_cor = x_hat(realLen, ip, i0, imageLen)
    z_cor = z_hat(dist, pixelLen, imageLen)
    dist  = (x_cor ** 2 + z_cor ** 2) ** 0.5
    return dist

  imHeight, imWidth = figDims
  center = (imWidth // 2, imHeight // 2)
  x, y, w, h   = boundingBox[0:4]
  
  relativeDist = measure        (dist = 0.51, realLen = 0.228, ip = x, jp = y, i0 = center[0], j0 = center[1], imageLen = w, pixelLen = 500)
  coordinates  = cal_coordinates(dist = 0.51, realLen = 0.228, ip = x, jp = y, i0 = center[0], j0 = center[1], imageLen = w, pixelLen = 500)
  return relativeDist, coordinates

## Testing (using Live Streaming)
detectLive2(model)

## Testing (using Untrained Images)
objectDetection(imageLoc + '/test/004.jpg')
objectDetection(imageLoc + '/test/008.jpg')
objectDetection(imageLoc + '/test/012.jpg')
objectDetection(imageLoc + '/test/016.jpg')
objectDetection(imageLoc + '/test/020.jpg')
objectDetection(imageLoc + '/test/024.jpg')
objectDetection(imageLoc + '/test/028.jpg')
objectDetection(imageLoc + '/test/032.jpg')
objectDetection(imageLoc + '/test/036.jpg')
objectDetection(imageLoc + '/test/040.jpg')
objectDetection(imageLoc + '/test/044.jpg')
objectDetection(imageLoc + '/test/048.jpg')
objectDetection(imageLoc + '/test/052.jpg')
objectDetection(imageLoc + '/test/056.jpg')
objectDetection(imageLoc + '/test/060.jpg')
objectDetection(imageLoc + '/test/064.jpg')
objectDetection(imageLoc + '/test/068.jpg')
objectDetection(imageLoc + '/test/072.jpg')
objectDetection(imageLoc + '/test/076.jpg')
objectDetection(imageLoc + '/test/080.jpg')
objectDetection(imageLoc + '/test/084.jpg')
objectDetection(imageLoc + '/test/085.jpg')

## Evaluation (using Trained Images)
objectDetection(imageLoc + '/train/001.jpg')
objectDetection(imageLoc + '/train/002.jpg')
objectDetection(imageLoc + '/train/003.jpg')
objectDetection(imageLoc + '/train/005.jpg')
objectDetection(imageLoc + '/train/006.jpg')
objectDetection(imageLoc + '/train/007.jpg')
objectDetection(imageLoc + '/train/009.jpg')
objectDetection(imageLoc + '/train/010.jpg')
objectDetection(imageLoc + '/train/011.jpg')
objectDetection(imageLoc + '/train/013.jpg')
objectDetection(imageLoc + '/train/014.jpg')
objectDetection(imageLoc + '/train/015.jpg')
objectDetection(imageLoc + '/train/017.jpg')
objectDetection(imageLoc + '/train/018.jpg')
objectDetection(imageLoc + '/train/019.jpg')
objectDetection(imageLoc + '/train/021.jpg')
objectDetection(imageLoc + '/train/022.jpg')
objectDetection(imageLoc + '/train/023.jpg')
objectDetection(imageLoc + '/train/025.jpg')
objectDetection(imageLoc + '/train/026.jpg')
objectDetection(imageLoc + '/train/027.jpg')
objectDetection(imageLoc + '/train/029.jpg')
objectDetection(imageLoc + '/train/030.jpg')
objectDetection(imageLoc + '/train/031.jpg')
objectDetection(imageLoc + '/train/033.jpg')
objectDetection(imageLoc + '/train/034.jpg')
objectDetection(imageLoc + '/train/035.jpg')
objectDetection(imageLoc + '/train/037.jpg')
objectDetection(imageLoc + '/train/038.jpg')
objectDetection(imageLoc + '/train/039.jpg')
objectDetection(imageLoc + '/train/041.jpg')
objectDetection(imageLoc + '/train/042.jpg')
objectDetection(imageLoc + '/train/043.jpg')
objectDetection(imageLoc + '/train/045.jpg')
objectDetection(imageLoc + '/train/046.jpg')
objectDetection(imageLoc + '/train/047.jpg')
objectDetection(imageLoc + '/train/049.jpg')
objectDetection(imageLoc + '/train/050.jpg')
objectDetection(imageLoc + '/train/051.jpg')
objectDetection(imageLoc + '/train/053.jpg')
objectDetection(imageLoc + '/train/054.jpg')
objectDetection(imageLoc + '/train/055.jpg')
objectDetection(imageLoc + '/train/057.jpg')
objectDetection(imageLoc + '/train/058.jpg')
objectDetection(imageLoc + '/train/059.jpg')
objectDetection(imageLoc + '/train/061.jpg')
objectDetection(imageLoc + '/train/062.jpg')
objectDetection(imageLoc + '/train/063.jpg')
objectDetection(imageLoc + '/train/065.jpg')
objectDetection(imageLoc + '/train/066.jpg')
objectDetection(imageLoc + '/train/067.jpg')
objectDetection(imageLoc + '/train/069.jpg')
objectDetection(imageLoc + '/train/070.jpg')
objectDetection(imageLoc + '/train/071.jpg')
objectDetection(imageLoc + '/train/073.jpg')
objectDetection(imageLoc + '/train/074.jpg')
objectDetection(imageLoc + '/train/075.jpg')
objectDetection(imageLoc + '/train/077.jpg')
objectDetection(imageLoc + '/train/078.jpg')
objectDetection(imageLoc + '/train/079.jpg')
objectDetection(imageLoc + '/train/081.jpg')
objectDetection(imageLoc + '/train/082.jpg')
objectDetection(imageLoc + '/train/083.jpg')
