import live_full_detection_detecto_with_ESP_CAM as live_feed

import serial
import time
PORT = "COM5" # for Alianware
serial_port = serial.Serial(PORT,115200)
serial_port.close()
serial_port.open()

gbx = 0
gby = 0
dist = 0
gbc = 0

### Declare Varables and Constants
device      = 'cuda'
imageLoc    = './images/train-test-4/'
trainLoc    = imageLoc + 'train/'
labelSet    = ['ball']
modelLoc    = './model_weights/'  #
modelFile   = 'model_weights-2-20210818_002355-cpu.pth'
modelAction = 'load'

model = live_feed.returnModel(modelAction, device, trainLoc, labelSet, modelLoc, modelFile)

waitTime = 0.1


while True:
    ## Testing (using Live Streaming)
    gbx, gby, dist  = live_feed.detectLive(model)
    intDist = int(dist) # cm
    if gbx != -1 and gby != -1:
        """
        value = 'gbx' + str(gbx) + '\n'
        print( value , end = '')
        serial_port.write(value.encode())
        time.sleep(waitTime)

        value = 'gby' + str(gby) + '\n'
        print( value)
        serial_port.write(value.encode())
        time.sleep(waitTime)
        """
        if gbx < 100:
            strgbx = '0' + str(gbx)
        else:
            strgbx = str(gbx)
        if gby < 100:
            strgby = '0' + str(gby)
        else:
            strgby = str(gby)
        if intDist < 100:
            strdist = '0' + str(intDist)
        else:
            strdist = str(intDist)


        value = 'gbc' + strgbx + strgby + strdist + '\n'    
        # value = 'gbc' + str(dist) + '\n'
        print(value)
        serial_port.write(value.encode())
        time.sleep(waitTime)

    else:
        value = 'gbx-1\n'
        serial_port.write(value.encode())
        time.sleep(waitTime)

        value = 'gby-1\n'
        serial_port.write(value.encode())
        time.sleep(waitTime)
        print("no green ball detected")
