## ONNX Convertor

import io
import onnx
import torch
import torch.onnx
import numpy as np
import onnxruntime
import torch.nn as nn
import torch.optim as optim
import torch.nn.init as init
import tensorflow_addons as tfa
import torch.nn.functional as F
import torch.utils.model_zoo as model_zoo

from torch import nn
from PIL import Image
from detecto import core
from torch.autograd import Variable
from IPython.display import display
from onnx_tf.backend import prepare
from torchvision import datasets, transforms

def modelName(type = 1):
  if type == 1:
    return 'fasterrcnn_resnet50_fpn'
  elif type == 2:
    return 'fasterrcnn_mobilenet_v3_large_fpn'
  elif type == 3:
    return 'fasterrcnn_mobilenet_v3_large_320_fpn'
  else:
    return ValueError('Unknown Pretrained Model')

device      = 'cpu'
imageLoc    = 'G:/Shared Drives/ECE 209  Blimp project/Images/train-test-4/'
labelSet    = ['ball']
modelLoc    = 'G:/Shared Drives/ECE 209  Blimp project/Ball Detection Model Weights/'
trainLoc    = imageLoc + 'train/'
minScore    =   0.50
colorLow    =  60
colorHigh   = 180
modelFile   = 'model_weights-2-20210818_002355-cpu.pth'
modelname   = modelName(int(modelFile[14]))
modelType   =   2
cropFactor  =   0.90
distWtsFile = './distance-detection-torch/distance-detection-weights-3-2.0sd-20210808_212550.json'
modelAction = 'load' # 'load' to load previous model, 'new' to train new model (only possible on Colab)
onnx_modelFile = modelFile[:-4] + '.onnx'


class SuperResolutionNet(nn.Module):
  def __init__(self, upscale_factor, inplace=False):
    super(SuperResolutionNet, self).__init__()

    self.relu = nn.ReLU(inplace=inplace)
    self.conv1 = nn.Conv2d(1, 64, (5, 5), (1, 1), (2, 2))
    self.conv2 = nn.Conv2d(64, 64, (3, 3), (1, 1), (1, 1))
    self.conv3 = nn.Conv2d(64, 32, (3, 3), (1, 1), (1, 1))
    self.conv4 = nn.Conv2d(32, upscale_factor ** 2, (3, 3), (1, 1), (1, 1))
    self.pixel_shuffle = nn.PixelShuffle(upscale_factor)

    self._initialize_weights()

  def forward(self, x):
    x = self.relu(self.conv1(x))
    x = self.relu(self.conv2(x))
    x = self.relu(self.conv3(x))
    x = self.pixel_shuffle(self.conv4(x))
    return x

  def _initialize_weights(self):
    init.orthogonal_(self.conv1.weight, init.calculate_gain('relu'))
    init.orthogonal_(self.conv2.weight, init.calculate_gain('relu'))
    init.orthogonal_(self.conv3.weight, init.calculate_gain('relu'))
    init.orthogonal_(self.conv4.weight)

  def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

torch_model = SuperResolutionNet(upscale_factor=3)
model_url   = 'modelLoc + modelFile'
batch_size  = 1
map_location = lambda storage, loc: storage
if torch.cuda.is_available():
  map_location = None
torch_model.load_state_dict(model_zoo.load_url(model_url, map_location=map_location))
torch_model.eval()

x = torch.randn(batch_size, 1, 224, 224, requires_grad=True)
torch_out = torch_model(x)
torch.onnx.export(torch_model,               # model being run
                  x,                         # model input (or a tuple for multiple inputs)
                  modelLoc + onnx_modelFile, # where to save the model (can be a file or file-like object)
                  export_params=True,        # store the trained parameter weights inside the model file
                  opset_version=10,          # the ONNX version to export the model to
                  do_constant_folding=True,  # whether to execute constant folding for optimization
                  input_names = ['input'],   # the model's input names
                  output_names = ['output'], # the model's output names
                  dynamic_axes={'input'  : {0 : 'batch_size'},   # variable length axes
                                'output' : {0 : 'batch_size'}})

onnx_model = onnx.load(modelLoc + onnx_modelFile)
onnx.checker.check_model(onnx_model)
ort_session = onnxruntime.InferenceSession(modelLoc + onnx_modelFile)
ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(x)}
ort_outs = ort_session.run(None, ort_inputs)
np.testing.assert_allclose(to_numpy(torch_out), ort_outs[0], rtol=1e-03, atol=1e-05)
print("Exported model has been tested with ONNXRuntime, and the result looks good!")

'''
model = core.Model(labelSet, modelname = modelname)
model._model.load_state_dict(torch.load(modelLoc + modelFile, map_location = torch.device(device)))
model.training = False
dummy_input = Variable(torch.randn(1, 1, 28, 28))
print(type(model))
onnx_modelFile = modelFile[:-4] + '.onnx'
torch.onnx.export(model, dummy_input, modelLoc + onnx_modelFile)
onnx_model = onnx.load(modelLoc + onnx_modelFile)

tf_rep = prepare(onnx_model)
tf_rep.export_graph(modelLoc + modelFile[:-4] + '.pb')
'''