load('FileDetails.mat');
boxData = [];
for n = drange(1:length(bndbox))
  bbox = cell2mat(bndbox(n));
  for i = 1:length(bbox)
      if bbox(i) == 0   
          bbox(i) = 1;
      end
  end
  s = size(bbox);
  boxData = [boxData; file(n,:), mat2cell(bbox, s(1), s(2))];
end
boxData = array2table(boxData);
boxData.Properties.VariableNames(1:2) = {'file','bndbox'};

doTraining = false;
if ~doTraining && ~exist('fasterRCNNResNet50EndToEndVehicleExample.mat','file')
    disp('Downloading pretrained detector (118 MB)...');
    pretrainedURL = 'https://www.mathworks.com/supportfiles/vision/data/fasterRCNNResNet50EndToEndVehicleExample.mat';
    websave('fasterRCNNResNet50EndToEndVehicleExample.mat',pretrainedURL);
end

rng(0)
shuffledIndices = randperm(height(boxData));
idx = floor(0.6 * height(boxData));

trainingIdx = 1:idx;
trainingDataTbl = boxData(shuffledIndices(trainingIdx),:);

validationIdx = idx+1 : idx + 1 + floor(0.1 * length(shuffledIndices) );
validationDataTbl = boxData(shuffledIndices(validationIdx),:);

testIdx = validationIdx(end)+1 : length(shuffledIndices);
testDataTbl = boxData(shuffledIndices(testIdx),:);

imdsTrain = imageDatastore(trainingDataTbl{:,'file'});
bldsTrain = boxLabelDatastore(trainingDataTbl(:,'bndbox'));

imdsValidation = imageDatastore(validationDataTbl{:,'file'});
bldsValidation = boxLabelDatastore(validationDataTbl(:,'bndbox'));

imdsTest = imageDatastore(testDataTbl{:,'file'});
bldsTest = boxLabelDatastore(testDataTbl(:,'bndbox'));

trainingData = combine(imdsTrain,bldsTrain);
validationData = combine(imdsValidation,bldsValidation);
testData = combine(imdsTest,bldsTest);

data = read(trainingData);
I = data{1};
bndbox = data{2};
bbox = [bndbox(1),bndbox(2),bndbox(3)-bndbox(1),bndbox(4)-bndbox(2)];
annotatedImage = insertShape(I,'Rectangle',bbox,'LineWidth',4);
annotatedImage = imresize(annotatedImage,2);
figure
imshow(annotatedImage)

inputSize = [224 224 3];
preprocessedTrainingData = transform(trainingData, @(data)preprocessData(data,inputSize));
numAnchors = 3;
anchorBoxes = estimateAnchorBoxes(preprocessedTrainingData,numAnchors)

featureExtractionNetwork = resnet50;
featureLayer = 'activation_40_relu';
numClasses = width(boxData)-1;
lgraph = fasterRCNNLayers(inputSize,numClasses,anchorBoxes,featureExtractionNetwork,featureLayer);

augmentedTrainingData = transform(trainingData,@augmentData);
augmentedData = cell(4,1);
for k = 1:4
    data = read(augmentedTrainingData);
    augmentedData{k} = insertShape(data{1},'Rectangle',data{2});
    reset(augmentedTrainingData);
end
figure
montage(augmentedData,'BorderSize',10)

trainingData = transform(augmentedTrainingData,@(data)preprocessData(data,inputSize));
validationData = transform(validationData,@(data)preprocessData(data,inputSize));

data = read(trainingData);

I = data{1};
bbox = data{2};
annotatedImage = insertShape(I,'Rectangle',bbox);
annotatedImage = imresize(annotatedImage,2);
figure
imshow(annotatedImage)

options = trainingOptions('sgdm',...
    'MaxEpochs',10,...
    'MiniBatchSize',2,...
    'InitialLearnRate',1e-3,...
    'CheckpointPath',tempdir,...
    'ValidationData',validationData);

if doTraining
    % Train the Faster R-CNN detector.
    % * Adjust NegativeOverlapRange and PositiveOverlapRange to ensure
    %   that training samples tightly overlap with ground truth.
    [detector, info] = trainFasterRCNNObjectDetector(trainingData,lgraph,options, ...
        'NegativeOverlapRange',[0 0.3], ...
        'PositiveOverlapRange',[0.6 1]);
else
    % Load pretrained detector for the example.
    pretrained = load('fasterRCNNResNet50EndToEndVehicleExample.mat');
    detector = pretrained.detector;
end

I = imread(testDataTbl.imageFilename{3});
I = imresize(I,inputSize(1:2));
[bboxes,scores] = detect(detector,I);

I = insertObjectAnnotation(I,'rectangle',bboxes,scores);
figure
imshow(I)

