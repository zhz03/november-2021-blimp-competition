import os
import numpy as np
import xml.etree.ElementTree as gfg 
from scipy.io import savemat

def ReadXML(inFile):
  tree   = gfg.parse(inFile)
  root   = tree.getroot()
  try:
    bndboxes = [root[6][4], root[7][4]]
  except:
    try:
      bndboxes = [root[6][4]]
    except:
      bndboxes = []
  return np.array([[int(val.text) for val in bndbox] for bndbox in bndboxes])

directory = 'G:\Shared drives\ECE 209  Blimp project\Images\\train-test-4\\train\\'
files     = sorted([(directory + f) for f in os.listdir(directory) if f[-4:] == '.jpg'])
bndboxes  = [ReadXML(file[:-4] + '.xml') for file in files]
savemat("./FileDetails.mat", {'file': files, 'bndbox': bndboxes})