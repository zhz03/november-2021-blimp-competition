import cv2
from urllib.request import urlopen, Request
import numpy as np

def nothing(x):
    pass


if __name__ == "__main__":
    #change the IP address below according to the
    #IP shown in the Serial monitor of Arduino code
    # url='http://192.168.4.1/cam-hi.jpg'
    # url='http://192.168.1.107/cam-hi.jpg'
    url='http://192.168.4.1/cam-mid.jpg'


    # cv2.namedWindow("live transmission", cv2.WINDOW_AUTOSIZE)

    cv2.namedWindow("live transmission", cv2.WINDOW_NORMAL)

    while True:
        header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36."}
        req = Request(url, headers=header)
        img_resp = urlopen(req, timeout=60)
        imgnp=np.array(bytearray(img_resp.read()),dtype=np.uint8)
        frame=cv2.imdecode(imgnp,-1)


        cv2.imshow("live transmission", frame)
        h,w,_ = frame.shape
        print("with:{},high:{}".format(w,h))
        key=cv2.waitKey(5)
        if key==ord('q'):
            break

    cv2.destroyAllWindows()
