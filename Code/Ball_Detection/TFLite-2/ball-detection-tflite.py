# Taken from https://github.com/NSTiwari/Custom-Object-Detection-on-Android-using-TF-Lite/blob/master/Custom_Object_Detection_using_TF_Lite_Model_Maker.ipynb

import numpy as np
import os
import glob
import PIL.Image

from tflite_model_maker.config import ExportFormat
from tflite_model_maker import model_spec
from tflite_model_maker import object_detector

import tensorflow as tf
assert tf.__version__.startswith('2')

tf.get_logger().setLevel('ERROR')
from absl import logging
logging.set_verbosity(logging.ERROR)

img = PIL.Image.open('/content/cartoon-detection/train/images/cartoon57.jpg')
img.format

spec = model_spec.get('efficientdet_lite2')

train_data = object_detector.DataLoader.from_pascal_voc("/content/cartoon-detection/train/images", "/content/cartoon-detection/train/annotations", ['doraemon', 'mrbean', 'scooby', 'mickey', 'mcqueen'])

validation_data = object_detector.DataLoader.from_pascal_voc('cartoon-detection/test/images', 'cartoon-detection/test/annotations', ['doraemon', 'mrbean', 'scooby', 'mickey', 'mcqueen'])

model = object_detector.create(train_data, model_spec=spec, batch_size=4, train_whole_model=True, validation_data=validation_data)

model.evaluate(validation_data)

model.export(export_dir='.')

model.evaluate_tflite('model.tflite', validation_data)
