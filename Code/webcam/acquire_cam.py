# -*- coding: utf-8 -*-
"""
Created on Sun Oct 10 22:59:21 2021

@author: Zhaoliang
"""

import cv2

def get_camera_quantity():
    """Acquire the available camera number"""
    quantity = 0
    cap = cv2.VideoCapture()  # video streaming 
    index = 0
    # normally, your laptop won't connect more than 5 camera
    while index < 5:
        ret = cap.open(index)
        if ret:
            quantity += 1  # available camera number + 1
            cap.release()  # release the camera
            index += 1  # index+1
        else:
        	break  # once fail to open, there is no more camera 
    return quantity


# run the following 2 lines of code first to make sure your connected camera
# is really available

"""
q = get_camera_quantity()
print(q)
"""

cap = cv2.VideoCapture(2)

while True:
    ret,frame = cap.read()
    if not ret:
        continue
    k = cv2.waitKey(10)
    if k== ord('q'):
        break
    cv2.imshow("image",frame)
cap.release()
cv2.destroyAllWindows()
