# Hardware and testing

## 1. Integrated system

### 1.1 Gondola

- Gondola **X8**
- Motor beams 2 long + 2 short **X8** 
- camera holder 9cm + camera holder 6 cm **X8**

### 1.2 Propulsion system

- ESP32 Feather board: master + slave  **X16**
- Motor shield **X8**
- 3 female JST-PH2 to 1 male JST-PH2 wires for motor shield **X8**
- 1 female JST-PH2 long wire for feather boad **X8**

### 1.3 ESP32-CAM

- esp32-cam **X16**
- voltage converters **X16**
- JST-HP female connecter **X16**
- Battery cases **X16**
- Batteries **X16**

### 1.4 Basket 

- Carbon fiber rods **X80**
- 3d printed materials **X80**
- Rectangle foam  **X8**
- Straws **X160**

## 2. Supply tools and materials

### 1.1 Laptop

- Alienware + power source  **X1**
- Aaron's laptop + power source  **X1**

### 1.2 Network supply 

- WIFI Router + power source **X1**
- Internet cable: to connect laptop with router **X4**

### 1.3 Tools

- Scissors **X6**
- Knife **X8**
- String **X4**
- Soldering iron
- small scissors
- hot glue gun

### 1.4 Other supplies

- Double sided tape

- Scotch tape

- Dotted tape

- Masking tape

- usb cables 

- usb extension hub 

- esp32-cam socket 

  
